package com.mxzh.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * SQLServer代码生成器
 *
 * @author wangx-e 975548620@qq.com
 * @since 2018-07-24
 */
@Mapper
public interface SQLServerGeneratorDao extends GeneratorDao {

}
