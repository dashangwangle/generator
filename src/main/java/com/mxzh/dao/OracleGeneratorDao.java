package com.mxzh.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * Oracle代码生成器
 *
 * @author wangx-e 975548620@qq.com
 * @since 2018-07-24
 */
@Mapper
public interface OracleGeneratorDao extends GeneratorDao {

}
